﻿using System;
using System.Collections.Generic;
using MySqlConnector;
using Xamarin.Forms;

namespace QuizApp
{
    public partial class GameView : ContentPage
    {
        private List<Template.Questions> Questions = new List<Template.Questions>();
        private string rightAnswer;

        public GameView()
        {
            InitializeComponent();
            MyData.setHighScore(0);
            getQuestions();
            nextQuestions();
        }

        void QuizBeenden(System.Object sender, System.EventArgs e)
        {
            try
            {
                using var connection = new MySqlConnection(MyData.getConnectionString());
                connection.Open();

                using var command = connection.CreateCommand();
                command.CommandText =
                    "INSERT into User(name, highscore) Value('" + MyData.getUserName() + "','" + MyData.getHighScore() +
                    "')";

                using var reader = command.ExecuteReader();

                command.Connection.Close();
            }
            catch (Exception err)
            {
                Console.WriteLine(err);
                throw;
            }

            MyData.setHighScore(0);
            Navigation.PopModalAsync();
        }

        void getQuestions()
        {
            try
            {
                using var connection = new MySqlConnection(MyData.getConnectionString());
                connection.Open();

                using var command = connection.CreateCommand();
                command.CommandText =
                    "SELECT id, prop, question, answ, answ1, answ2, answ3, answ4 FROM Questions"; // noinspection SqlResolveForFile


                using var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Questions.Add(new Template.Questions()
                    {
                        id = reader.GetInt32("id"),
                        prop = reader.GetInt32("prop"),
                        question = reader.GetString("question"),
                        answ = reader.GetInt32("answ"),
                        answ1 = reader.GetString("answ1"),
                        answ2 = reader.GetString("answ2"),
                        answ3 = reader.GetString("answ3"),
                        answ4 = reader.GetString("answ4"),
                    });
                }

                command.Connection.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void nextQuestions()
        {
            punkteAnzeige.Text = "Punkte: " + MyData.getHighScore().ToString();
            Random r = new Random();
            Template.Questions q = Questions[r.Next(Questions.Count)];
            rightAnswer = q.answ1;


            questionLabel.Text = q.question;
            answ1Label.Text = q.answ1;
            answ2Label.Text = q.answ2;
            answ3Label.Text = q.answ3;
            answ4Label.Text = q.answ4;
        }

        private void AnswLabel_OnClicked(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            if (btn.Text == rightAnswer)
            {
                Correct();
            }
            else
            {
                Wrong();
            }
        }

        void Correct()
        {
            DisplayAlert("Richtig", "Deine gegebene Antwort scheint richtig zu sein!", "OK");
            MyData.addHighScore();
            nextQuestions();
        }

        void Wrong()
        {
            DisplayAlert("Falsch", "Leider Falsch. Bitte versuche es das nächste mal erneut", "OK");
            nextQuestions();
        }
    }
}