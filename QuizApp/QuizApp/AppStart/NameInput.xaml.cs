using System;
using QuizApp.AppStart;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuizApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NameInput : ContentPage
    {
        public NameInput()
        {
            InitializeComponent();
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            if (Entry.Text != null && Entry.Text != "" && Entry.Text != " ")
            {
                MyData.setUserName(Entry.Text);
                Navigation.PushAsync(new QuizSetup());
            }
            else
            {
                DisplayAlert("Fehler:", "Bitte gib einen Namen ein", "OK");
            }
        }
    }
}