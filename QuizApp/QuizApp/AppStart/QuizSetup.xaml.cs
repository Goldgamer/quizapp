using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuizApp.AppStart
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizSetup : ContentPage
    {
        public QuizSetup()
        {
            InitializeComponent();
        }

        private void Transition()
        {
            Navigation.PushAsync(new MainPage());
        }


        private void Flaggen(object sender, EventArgs e)
        {
            MyData.setTopic(0);
            Transition();
        }

        private void Ländernamen(object sender, EventArgs e)
        {
            MyData.setTopic(1);
            Transition();
        }

        private void Hauptstädte(object sender, EventArgs e)
        {
            MyData.setTopic(2);
            Transition();
        }
    }
}