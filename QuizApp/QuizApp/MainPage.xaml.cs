﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using MySqlConnector;
using Xamarin.Forms;

namespace QuizApp
{
    public partial class MainPage : ContentPage
    {
        private readonly string userName = MyData.getUserName();
        List<Template.User> Users = new List<Template.User>();

        public MainPage()
        {
            refreshUsers();
            InitializeComponent();
            UsersList.ItemsSource = Users;
            welcomeLabel.Text = "Willkommen " + userName;
            topic.Text = "Dein Thema lautet: " + getTopicString();
        }

        private void refreshUsers()
        {
            using var connection = new MySqlConnection(MyData.getConnectionString());
            connection.Open();

            using var command = connection.CreateCommand();
            command.CommandText = "SELECT id, name, highscore FROM User"; // noinspection SqlResolveForFile


            using var reader = command.ExecuteReader();
            while (reader.Read())
            {
                Users.Add(new Template.User()
                {
                    id = reader.GetInt32("id"),
                    name = reader.GetString("name"),
                    highscore = reader.GetInt32("highscore"),
                });
            }


            Users = Users.OrderBy(p => p.highscore).ToList();
            Users.Reverse();
        }

        public string getTopicString()
        {
            switch (MyData.getTopic())
            {
                case 0:
                    return "Flaggen";
                case 1:
                    return "Ländernamen";
                case 2:
                    return "Hauptstädte";
                default:
                    return null;
            }
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new GameView());
        }
    }
}