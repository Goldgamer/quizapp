create schema QuizApp;

use QuizApp;

create table Questions
(
    id       int primary key not null auto_increment,
    prop     int             not null,
    question varchar(100)    not null,
    answ     int             not null,
    answ1    varchar(100)    not null,
    answ2    varchar(100)    not null,
    answ3    varchar(100)    not null,
    answ4    varchar(100)    not null
);


create table User
(
    id        int primary key not null auto_increment,
    name      varchar(40)     not null,
    highscore int             not null
);

insert into User (name, highscore)
    VALUE
    ('Max', 1000),
    ('Peter', 5),
    ('Gustavo', 400);

# 0 - Flaggen

insert into Questions (prop, question, answ, answ1, answ2, answ3, answ4)
    VALUE
    (0, 'Welches Land hat die Schwarz-Rot-Gold Flagge?', 1, 'Deutschland', 'Frankreich', 'Griechenland', 'Japan');