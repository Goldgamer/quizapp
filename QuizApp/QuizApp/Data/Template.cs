namespace QuizApp
{
    public struct Template
    {
        public struct User
        {
            public int id { get; set; }
            public string name { get; set; }
            public int highscore { get; set; }
        }

        public struct Questions
        {
            public int id { get; set; }

            public int prop { get; set; }

            public string question { get; set; }

            public int answ { get; set; }

            public string answ1 { get; set; }

            public string answ2 { get; set; }

            public string answ3 { get; set; }

            public string answ4 { get; set; }
        }
    }
}